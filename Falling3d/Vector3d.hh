#ifndef FALLING3D_VECTOR3D_HH
# define FALLING3D_VECTOR3D_HH

# include <iostream>

# include <Falling/Algebra/CrossProd.hh>
# include <Falling/Algebra/Vect.hh>

namespace Falling
{
  namespace Algebra
  {
    template <>
      struct CrossProdTrait<Vect<3>>
      {
        static const bool value = true;

        typedef Vect<3> ResultType;

        static Vect<3> cross(const Vect<3>& a, const Vect<3>& b)
        {
          Vect<3> res;

          res.components[0] = a.components[1] * b.components[2]
                              - a.components[2] * b.components[1];
          res.components[1] = a.components[2] * b.components[0]
                              - a.components[0] * b.components[2];
          res.components[2] = a.components[0] * b.components[1]
                              - a.components[1] * b.components[0];

          return res;
        }
      };
  } // end Algebra
} // end Falling
#endif
