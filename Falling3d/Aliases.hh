#ifndef FALLING3D_ALIASES
# define FALLING3D_ALIASES

# include <Falling/Metal/Inherit.hh>
# include <Falling/Metal/Sort.hh>

# include <Falling/Algebra/OrthogonalMatrix.hh>
# include <Falling/Algebra/Vect.hh>
# include <Falling/Algebra/Error.hh>

# include <Falling/Collision/Contact.hh>
# include <Falling/Collision/Detection/BroadPhase/BruteForceBroadPhase.hh>
# include <Falling/Collision/Detection/BroadPhase/BruteForceLoozeBoundingVolumeBroadPhase.hh>
# include <Falling/Collision/Detection/BroadPhase/DynamicLoozeBoundingVolumeHierarchyBroadPhase.hh>
# include <Falling/Collision/Detection/NarrowPhase/AbstractNarrowPhase.hh>
# include <Falling/Collision/Detection/NarrowPhase/DefaultCollisionDispatcher.hh>
# include <Falling/Collision/Detection/NarrowPhase/JohnsonSimplex.hh>
# include <Falling/Collision/Detection/NarrowPhase/GJK.hh>
# include <Falling/Collision/Graph/AllNarrowPhaseAccumulator.hh>

# include <Falling/Constraint/Solver/AccumulatedImpulseConstraintSolver.hh>
# include <Falling/Constraint/Solver/IndexProxy.hh>

# include <Falling/Integrator/BodyGravityIntegrator.hh>

# include <Falling/Body/RigidBodyIntegrator.hh>
# include <Falling/Body/RigidBody.hh>
# include <Falling/Body/RigidBodyCollisionDispatcher.hh>
# include <Falling/Body/RigidBodyIndexer.hh>

# include <Falling/Shape/ImplicitShape/HasBoundingVolumeAABB.hh>

# include <Falling/Shape/Ball/Ball.hh>
# include <Falling/Shape/Ball/ImplicitShape.hh>
# include <Falling/Shape/Ball/TransformableShape.hh>
# include <Falling/Shape/Ball/HasBoundingVolumeAABB.hh>
# include <Falling/Shape/Ball/HasCenterOfMass.hh>

# include <Falling/Shape/Plane/Plane.hh>
# include <Falling/Shape/Plane/TransformableShape.hh>
# include <Falling/Shape/Plane/HasBoundingVolumeAABB.hh>

# include <Falling/Shape/Box/Box.hh>
# include <Falling/Shape/Box/ImplicitShape.hh>
# include <Falling/Shape/Box/TransformableShape.hh>
# include <Falling/Shape/Box/HasCenterOfMass.hh>

# include <Falling/Shape/TransformedShape/TransformedShape.hh>
# include <Falling/Shape/TransformedShape/ImplicitShape.hh>
# include <Falling/Shape/TransformedShape/TransformableShape.hh>
# include <Falling/Shape/TransformedShape/HasCenterOfMass.hh>
# include <Falling/Shape/TransformedShape/VolumetricShape.hh>

# include <Falling/Shape/CSO/ImplicitShape.hh>
# include <Falling/Shape/CSO/HasCenterOfMass.hh>

# include <Falling/BoundingVolume/AABB/HasCenterOfMass.hh>
# include <Falling/BoundingVolume/AABB/BoundingVolume.hh>
# include <Falling/BoundingVolume/AABB/LoozeBoundingVolume.hh>

# include <Falling/World/GenericWorld.hh>

# include <Falling3d/Shape3d.hh>
# include <Falling3d/Vector3d.hh>
# include <Falling3d/Matrix3d.hh>

namespace Falling3d
{
  using namespace Falling;
  using namespace Falling::Algebra;
  using namespace Falling::Integrator;
  using namespace Falling::Body;
  using namespace Falling::World;
  using namespace Falling::Shape;
  using namespace Falling::Collision;
  using namespace Falling::Metal;
  using namespace Falling::Collision::Detection::BroadPhase;
  using namespace Falling::Collision::Detection::NarrowPhase;
  using namespace Falling::Constraint::Solver;


  using Vect3d      = Vect<3>;
  using Transform3d = OrthogonalMatrix<3>;
  using BoundingVolume3d = AABB<Vect3d>;
  using Shape3d     = InheritLattice<
                        Metal::Dynamic::Identifiable
                        , BoundingVolume::Dynamic::HasBoundingVolume<BoundingVolume3d>
                      >;

  using Body3d = RigidBody<Shape3d
                           , Transform3d
                           , Vect3d
                           , Vect3d
                           , IndexProxy
                           , BVHTreeNodeProxy<BoundingVolume3d>
                           >; //, LoozeBoundingVolumeProxy<BoundingVolume3d>>;

  template <typename T> 
    using PartialBodyIntegrator = BodyGravityIntegrator<T, Vect3d, Vect3d>;

  using BodyGravityIntegrator3d = BodyGravityIntegrator<Body3d, Vect3d, Vect3d>;
  using RigidBodyGravityIntegrator3d = RigidBodyIntegrator<PartialBodyIntegrator, Body3d>;
  using BroadPhase3d = DynamicLoozeBoundingVolumeHierarchyBroadPhase<Body3d, BoundingVolume3d>;
                                 // BruteForceLoozeBoundingVolumeBroadPhase<Body3d, BoundingVolume3d>;
                                 // BruteForceBroadPhase<Body3d>;
  using GJKSimplex = JohnsonSimplex<AnnotatedSupportPoint<Vect3d>>;

  using SubCollisionDispatcher3d = 
    DefaultCollisionDispatcher<Vect3d, Transform3d, GJKSimplex, Error<double>, 50>;

  using CollisionDispatcher3d = RigidBodyCollisionDispatcher<SubCollisionDispatcher3d, Body3d>;
  using NarrowPhase3d = AbstractNarrowPhase<
                          Contact<Vect3d>
                          , Metal::ConstInheritLattice<Metal::Dynamic::Identifiable>::type
                        >;
  using Contact3d   = Contact<Vect3d>;
  using Accumulator3d = AllNarrowPhaseAccumulator<
                          Body3d
                          , NarrowPhase3d
                          , Contact3d
                          , Metal::ConstInheritLattice<Metal::Dynamic::Identifiable>::type>;

  using Ball3d  = Ball<Vect3d>;
  using Box3d   = Box<Vect3d>;
  using Plane3d = Plane<Vect3d>;

  using ConstraintsSolver3d = AccumulatedImpulseConstraintSolver<Body3d,
                                                                 Vect3d,
                                                                 Vect3d,
                                                                 RigidBodyIndexer<Body3d>>;

  using World3d = GenericWorld<Body3d
                               , RigidBodyGravityIntegrator3d
                               , NarrowPhase3d
                               , BroadPhase3d
                               , CollisionDispatcher3d
                               , Accumulator3d
                               , ConstraintsSolver3d>;
}

#endif // FALLING3D_ALIASES
