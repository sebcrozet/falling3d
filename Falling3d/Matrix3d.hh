#ifndef FALLING3D_MATRIX3D
# define FALLING3D_MATRIX3D

# include <Falling/Algebra/OrthogonalMatrix.hh>
# include <Falling/Algebra/Rotatable.hh>

namespace Falling
{
  namespace Algebra
  {
    template <>
      struct RotatableTrait<OrthogonalMatrix<3>>
      {
        static const bool value = true;

        typedef Vect<3> RotationType;

        static Vect<3> rotation(const OrthogonalMatrix<3>& mat)
        {
          Vect<3> res;

          // res.components[0] = -atan(mat.rotation()[0] / mat.rotation()[1]);

          return res;
        }

        static OrthogonalMatrix<3>& rotate(const Vect<3>& ang, OrthogonalMatrix<3>& mat)
        {
          // double              siteta = sin(ang.components[0]);
          // double              coteta = cos(ang.components[0]);
          // OrthogonalMatrix<3> rotmat;
          // rotmat.rotation()[0] = coteta;
          // rotmat.rotation()[1] = -siteta;
          // rotmat.rotation()[2] = siteta;
          // rotmat.rotation()[3] = coteta;

          // mat = mat * rotmat;

          return mat;
        }
      };
  } // end Matrix3d
} // end Falling3d

#endif // FALLING3D_MATRIX3D
