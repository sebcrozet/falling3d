#ifndef FALLING3D_SHAPE3D
# define FALLING3D_SHAPE3D

# include <Falling/Algebra/Vect.hh>
# include <Falling/Shape/Ball/Ball.hh>
# include <Falling/Shape/Box/Box.hh>

namespace Falling
{
  namespace Shape
  {
    template <>
      struct VolumetricShapeTrait<Ball<Vect<3u>>>
      {
        static const bool value = true;

        typedef OrthogonalMatrix<3u> InertiaTensorType;

        static double volume(const Ball<Vect<3u>>& ball)
        {
          return M_PI * ball.radius() * ball.radius() * ball.radius();;
        }

        static InertiaTensorType inertiaTensor(const Ball<Vect<3u>>& ball,
                                               double                mass)
        {
          InertiaTensorType res  = AbelianGroupTrait<OrthogonalMatrix<3u>>::zero;
          double            diag = 2.0 * mass * ball.radius() * ball.radius() / 5.0;

          res.rotationAt(0, 0) = diag;
          res.rotationAt(1, 1) = diag;
          res.rotationAt(2, 2) = diag;

          return res;
        }
      };

    template <>
      struct VolumetricShapeTrait<Box<Vect<3u>>>
      {
        static const bool value = true;

        typedef OrthogonalMatrix<3u> InertiaTensorType;

        static double volume(const Box<Vect<3u>>& box)
        {
          return 8.0 * box.halfExtents()[0] * box.halfExtents()[1] * box.halfExtents()[2];
        }

        static InertiaTensorType inertiaTensor(const Box<Vect<3u>>& box,
                                               double               mass)
        {
          InertiaTensorType res  = AbelianGroupTrait<OrthogonalMatrix<3u>>::zero;

          res.rotationAt(0, 0) = mass * (box.halfExtents()[1] * box.halfExtents()[1]
                                         + box.halfExtents()[2] * box.halfExtents()[2]) / 12.0;
          res.rotationAt(1, 1) = mass * (box.halfExtents()[0] * box.halfExtents()[0]
                                         + box.halfExtents()[2] * box.halfExtents()[2]) / 12.0;
          res.rotationAt(2, 2) = mass * (box.halfExtents()[0] * box.halfExtents()[0]
                                         + box.halfExtents()[1] * box.halfExtents()[1]) / 12.0;

          return res;
        }
      };
  }
}

#endif // FALLING3D_SHAPE3D
